// --- APPELER UNE API AVEC JQUERY 666 //

// let callBack = function (data) {
//     console.log(data)
//     document.getElementById("my_Pokemon").innerHTML = data.id + " " + data.name.toUpperCase();

//     if (data.habitat == null) {
//         document.getElementById("zone_Pokemon").innerHTML = "this pokemon is legendary we don't know where he lives";
//     } else {
//         document.getElementById("zone_Pokemon").innerHTML = "this pokemon live in the " + data.habitat.name;
//     }


//     document.getElementById("img_Pokemon").src = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + data.id + ".png";
//     document.getElementById("img_Pokemon").style.display = "block";

//     for (let i = 0; i < data.flavor_text_entries.length; i++) {
//         if (data.flavor_text_entries[i].language.name === "en") {
//             return document.getElementById("describe_Pokemon").innerHTML = data.flavor_text_entries[i].flavor_text;
//         }

//     }
// };


// function pokemonInfo() {
//     let pokemon = document.getElementById("pokemon").value;
//     let url = "https://pokeapi.co/api/v2/pokemon-species/" + pokemon;


//     $.get(url, callBack).done(function () {

//     })

//         .fail(function () {
//             alert('error');
//         })

//         .always(function () {

//         });
// };






// --- APPELER UNE API AVEC LA METHOD FETCH --- //

function pokemonInfo() {


    let pokemon = document.getElementById("pokemon").value;
    let url = "https://pokeapi.co/api/v2/pokemon-species/" + pokemon;

    fetch(url)
        .then(response => {
            return response.json();
        })
        .then(data => {
            console.log(data);
            document.getElementById("my_Pokemon").innerHTML = "N°" + data.id + " " + data.name.toUpperCase();

            if (data.habitat == null) {
                document.getElementById("zone_Pokemon").innerHTML = "this pokemon is legendary we don't know where he lives";
            } else {
                document.getElementById("zone_Pokemon").innerHTML = "this pokemon live in the " + data.habitat.name;
            }


            document.getElementById("img_Pokemon").src = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + data.id + ".png";
            document.getElementById("img_Pokemon").style.display = "block";

            for (let i = 0; i < data.flavor_text_entries.length; i++) {
                if (data.flavor_text_entries[i].language.name === "en") {
                    return document.getElementById("describe_Pokemon").innerHTML = data.flavor_text_entries[i].flavor_text;
                }

            }
        })
        .catch(data => {
            alert('erreur tu a mal écris ta recherche');
        })


}
